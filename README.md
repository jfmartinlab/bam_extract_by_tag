# Bam Extract by Tag
Extract reads from bam file by tags

For when you want to split a bam file up by reads that match a certain tag. This script will accept one bam file, one tag, and multiple tag files each of which contain the tag value to match against. Each tag value must be on it's own line.

## Installation
Requires pysam, which should be installed automatically as a dependency.

```sh
pip install git+https://gitlab.com/jfmartinlab/bam_extract_by_tag
```

I recommend a conda environment or a virtual environment.
```sh
conda create -n bam_extraction 'python>=3.6' pip && conda activate bam_extraction && pip install git+https://gitlab.com/jfmartinlab/bam_extract_by_tag
```

## Usage
```sh
usage: bam_extract_by_tag [-h] [--version] -t TAG [-y TAG_TYPE] [-c COMP_THREADS] [-o OUTPUT_DIR]
                          [-s OUTPUT_SUFFIX] [--no_unaligned_reads] [-q]
                          bam_file tag_files [tag_files ...]
```

For example:
```sh
bam_extract_by_tag -t CB -y Z input.bam barcodes1.txt barcodes2.txt barcodes3.txt
```
Creates three new bam files called `barcodes1.txt.extracted.bam`, `barcodes2.txt.extracted.bam`, and `barcodes3.txt.extracted.bam`.
It splits `input.bam` based on the tag `CB` with the tag type `Z`. Each read that has a tag value in one of the tag files will be writted to its respective bam file.
