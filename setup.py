import setuptools

from bam_extract_by_tag import __version__

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="bam_extract_by_tag",
    version=__version__,
    author="Thomas Martin",
    author_email="tjmartin022@gmail.com",
    description="Extract reads from bam file by tags. (or) Split bam files by tags.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jfmartinlab/bam_extract_by_tag",
    packages=setuptools.find_packages(),
    scripts=["bin/bam_extract_by_tag"],
    install_requires=[
        "pysam==0.16.0.1",
      ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
