#!/usr/bin/env python3
"""
Extract reads from bam file by tags

For when you want to split a bam file up by reads that match a certain tag. 
This script will accept one bam file, one tag, and multiple tag files each of 
which contain the tag value to match against. Each tag value must be on it's own line.
"""

import sys
import os
import argparse
from dataclasses import dataclass
from contextlib import ExitStack

import pysam

from bam_extract_by_tag import __version__

@dataclass
class TagData:
    """Class for keeping track of a tag file, it's contents and it's outputs."""
    name: str

    values: set
    matching_tag_count: int # matching tag value counts for stats

    out_name: str
    out_bam_path: str
    out_bam_file: pysam.AlignmentFile = None

    def __init__(self, tag_file_path: str, out_dir: str, out_suffix: str):
        self.name = os.path.basename(tag_file_path)

        with open(tag_file_path, 'r') as t_file:
            self.values = set(line.strip() for line in t_file)

        self.matching_tag_count = 0

        self.out_name = self.name + out_suffix
        self.out_bam_path = os.path.join(out_dir, self.out_name)

        # field to hold file handle, empty for now
        self.out_bam_file = None


def parse_arguments(argv):
    """Parse command line arguments using argparse"""

    parser = argparse.ArgumentParser(description = "Extract reads from bam file by tags.")

    parser.add_argument("--version", action="version", version=f"%(prog)s {__version__}")

    # https://pysam.readthedocs.io/en/latest/api.html#pysam.AlignedSegment.get_tag
    # https://samtools.github.io/hts-specs/SAMv1.pdf
    parser.add_argument("-t", "--tag", required = True,
                        help = "Tag to use. Possible values 'AcCsSiIfZHB' (see BAM format specification)")
    parser.add_argument("-y", "--tag_type", default = None,
                        help = "Optional tag type to use")

    # https://pysam.readthedocs.io/en/latest/api.html#pysam.AlignmentFile
    # the threads option
    parser.add_argument("-c", "--comp_threads", default = 2, type = int,
                        help = """
                        (de)compression threads per bam file. This includes the input bam and all output bams.
                        Default is 2.
                        """)

    parser.add_argument("-o", "--output_dir", default = os.getcwd(),
                        help = "Path to output directory. Default is current working directory.")
    parser.add_argument("-s", "--output_suffix", default = ".extracted.bam",
                        help = "What to append to the output file names. Default is '.extracted.bam'")

    # https://pysam.readthedocs.io/en/latest/faq.html?highlight=fetch#alignmentfile-fetch-does-not-show-unmapped-reads
    # https://pysam.readthedocs.io/en/latest/faq.html?highlight=fetch#bam-files-with-a-large-number-of-reference-sequences-are-slow
    parser.add_argument("--no_unaligned_reads", dest = "do_unaligned_reads", action = "store_false",
                        help = "Don't match tag to unaligned reads. Requires indexed bam file. Could be slower on large bam files.")
    parser.set_defaults(do_unaligned_reads = True)
    parser.add_argument("-q", "--quiet", default = False, action = "store_true",
                        help = "No progress or stats on standard output.")

    parser.add_argument("bam_file",
                        help = "Bam file to extract from")
    parser.add_argument("tag_files", nargs = "+",
                        help = """
                        Tag files that each have a list of tag values to match with. 
                        Each tag value must be on it's own line.
                        Don't include the tag itself. So no CB: prepended for example.
                        """)

    args = parser.parse_args(argv)

    if args.comp_threads <= 0:
        parser.print_usage()
        print("Error: Threads <= 0.", file = sys.stderr)
        sys.exit(1)

    if not os.path.isdir(args.output_dir):
        parser.print_usage()
        print(f"Error: Invalid Output Directory ({args.output_dir})", file = sys.stderr)
        sys.exit(1)

    # otherwise it would overwrite the tag file
    if len(args.output_suffix) == 0:
        parser.print_usage()
        print("Error: Empty output_suffix.", file = sys.stderr)
        sys.exit(1)

    return args


def main(argv = sys.argv[1:]):
    args = parse_arguments(argv)
    #print(args)

    tag_data = []
    for tag_file in args.tag_files:
        tag_data.append(TagData(tag_file, args.output_dir, args.output_suffix))

    matching_tag_count = 0 # total reads containing the tag

    # open all bamfiles in the same context manager
    with ExitStack() as stack:
        bamfile = stack.enter_context(
            pysam.AlignmentFile(args.bam_file, "rb", threads = args.comp_threads))
        if not args.quiet:
            print("Creating Bam files:")

        for tag_file in tag_data:
            if not args.quiet:
                print('\t' + tag_file.out_bam_path)
            tag_file.out_bam_file = stack.enter_context(
                pysam.AlignmentFile(tag_file.out_bam_path, "wb", template = bamfile, threads = args.comp_threads))

        if not args.quiet:
            print("\nWriting reads with matching tag values...")
        # the main loop; loops on each read and decides if and where to write the read to.
        for read in bamfile.fetch(until_eof = args.do_unaligned_reads):
            try:
                if args.tag_type is None:
                    tag_value = read.get_tag(args.tag)
                else:
                    tag_value, tag_type = read.get_tag(args.tag, with_value_type = True)
                    if args.tag_type != tag_type:
                        continue
            except KeyError: # tag isn't in this read
                continue

            matching_tag_count += 1

            # if we got to here the read contains the tag.
            # time to see if the tag value matches anything
            for tag_file in tag_data:
                if tag_value in tag_file.values:
                    tag_file.out_bam_file.write(read)
                    tag_file.matching_tag_count += 1

    if not args.quiet:
        print("Done.\n")
        print(f"Reads with matching tag:\t{str(matching_tag_count)}")
        print("Reads with matching tag value per file:")
        for tag_file in tag_data:
            print(f"\t{tag_file.out_name}\t{str(tag_file.matching_tag_count)}")


if __name__ == "__main__":
    main()
